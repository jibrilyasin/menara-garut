<?php

use Illuminate\Support\Facades\Route;
use App\Models\Districts;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/clear-config', function() {
  try {
    $query = Districts::select(DB::raw('AsText(ogc_geom) as ogc_geom'))->first();
    $polygon = MultiPolygon::fromWKT($query['ogc_geom']);
    $arr = ['type'=>"FeatureCollection",'features'=>[['type'=>'Feature','geometry'=>$polygon,'properties'=>['code'=>'12345','name'=>'Wiwitan']]]];
    return $arr;
    $response = [
      'status' => 'success',
      'message' => 'Proses berhasil',
      'getdata' => $polygon,
    ];
    return response()->json($response, 200);
  } catch (\Exception $e) {
    $response = [
      'status' => 'error',
      'message' => $e->getMessage(),
    ];
    return response()->json($response, 500);
  }
});
Route::get('/migrate', function() {
	$exitCode = Artisan::call('migrate');
	return '<h1>Migrate</h1>';
});

Route::get('/auth/login','Auth\AuthController@login');
Route::get('/auth/forgot','Auth\AuthController@forgot');

Route::get('/{any}',function(){
	return view('layouts.dashboard');
})->where('any','.*');
