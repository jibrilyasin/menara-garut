<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/login','Auth\AuthController@login');

Route::group(['middleware' => 'auth:sanctum'], function() {
  // Account
  Route::group(['prefix'=>'myaccount'],function () {
  	Route::get('/getprofile','Auth\ProfileController@getProfile');
    Route::post('/password','Auth\ProfileController@postPassword');
    Route::post('/profile','Auth\ProfileController@postProfile');
  });
  // End

  Route::resource('tower/districts', 'Tower\DistrictsController')->except([
    'index','destroy'
  ]);
  Route::group(['prefix'=>'tower/districts'],function () {
    Route::post('/list','Tower\DistrictsController@index');
  });

  // Logout
  Route::post('logout','Auth\AuthController@logout');
  // End
});
