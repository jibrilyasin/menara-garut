<?php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use App\User;

class myFunction
{
  public static function id($table, $field)
  {
    $data = DB::table($table)->max($field);
    return $data + 1;
  }
  public static function getAssets()
  {
    if (!empty($_SERVER['HTTP_HOST'])) {
      $hostName = $_SERVER['HTTP_HOST'];
      if (\Request::getHttpHost() == 'localhost') {
        $protocol = 'http://';
      } else {
        $protocol = 'https://';
      }
      $url = $protocol . $hostName . dirname($_SERVER['PHP_SELF']);
      return $url;
    }
  }
  public static function baseURL()
  {
    if (\Request::getHttpHost() == 'localhost') {
      $baseurl = 'http://localhost:8000';
    } else {
      $protocol = 'https://';
      $getslash = explode("/", dirname($_SERVER['PHP_SELF']));
      if (!empty($getslash[1])) {
        $baseurl = $protocol . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
      } else {
        $baseurl = $protocol . $_SERVER['HTTP_HOST'];
      }
    }
    return $baseurl;
  }
  public static function getSource()
  {
    if (\Request::getHttpHost() == 'localhost') {
      $protocol = 'http://';
    } else {
      $protocol = 'https://';
    }
    $getslash = explode("/", dirname($_SERVER['PHP_SELF']));
    if (!empty($getslash[1])) {
      //$baseurl=$protocol.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
      if (\Request::getHttpHost() == 'localhost') {
        $baseurl = $protocol . $_SERVER['HTTP_HOST'] . '/' . $getslash[1] . '/' . $getslash[2] . '/public/source/';
      } else {
        $baseurl = $protocol . $_SERVER['HTTP_HOST'] . '/public/source/';
      }
    } else {
      $baseurl = $protocol . $_SERVER['HTTP_HOST'];
    }

    return $baseurl;
  }
  public static function highlight($datatext, $datakeyword)
  {
    if (!empty($datakeyword)) {
      $text = $datatext;
      $keyword = $datakeyword;
      $pattern = str_replace(' ', '|', $keyword);
      $result = preg_replace("/$pattern/i", '<code>\0</code>', $text);
    } else {
      $result = $datatext;
    }
    return $result;
  }

  public static function color_badges_status($param)
  {
    switch ($param) {
      case 'Success':
        return "success";
        break;
      case 'Failed':
        return "danger";
        break;
      case 'Pending':
        return "warning";
        break;
      case 'Setor':
        return "secondary";
        break;
      case 'Bayar':
        return "primary";
        break;
      case 'Active':
        return "info";
        break;
      case 'Paid':
        return "light";
        break;
      case 'Expired':
        return "dark";
        break;
    }
  }
  public static function formatNumber($amount, $decimal = 0)
  {
    return number_format($amount, $decimal, '.', ',');
    //return $prefix . number_format($amount, 0, ',', '.');
  }
}
