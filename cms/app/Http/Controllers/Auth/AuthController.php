<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

use App\User;
use App\Models\Branch;
use Hash;

class AuthController extends Controller
{
  public function login(Request $request)
  {
    if ($request->isMethod('post')) {
      $validate = \Validator::make($request->all(), [
        'email' => 'required',
        'password' => 'required|min:5',
      ]);
      if ($validate->fails()) {
        $response = [
          'status' => 'error',
          'message' => 'Validator error',
          'errors' => $validate->errors(),
        ];
        return response()->json($response, 422);
      } else {
        $credentials = request(['email', 'password']);
        $credentials = Arr::add($credentials, 'active', 'Y');
        if (!Auth::attempt($credentials)) {
          $response = [
            'status' => 'error',
            'message' => 'Mohon periksa kembali Email atau Password anda.',
          ];
          return response()->json($response, 200);
        }
        $user = User::where('email', $request->email)->first();

        $tokenResult = $user->createToken('token-auth')->plainTextToken;
        $response = [
          'status' => 'success',
          'message' => 'Proses login berhasil.',
          'content' => [
            'access_token' => $tokenResult,
          ],
        ];
        return response()->json($response, 200);
      }
    } else {
      return view('layouts.auth');
    }
  }
  public function logout(Request $request)
  {
    $user = $request->user();
    try {
      $user->currentAccessToken()->delete();
      $response = [
        'status' => 'success',
        'msg' => 'Logout successfully'
      ];
      return response()->json($response, 200);
    } catch (\Exception $e) {
      $response = [
        'status' => 'error',
        'message' => $e->getMessage(),
      ];
      return response()->json($response, 500);
    }
  }
}
