<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Hash;

class ProfileController extends Controller
{
  public function getProfile(Request $request)
  {
    try {
      $query = User::where('id', $request->user()->id)->first();
      $response = [
        'status' => 'success',
        'message' => 'Request Successfully',
        'getdata' => $query,
      ];
      return response()->json($response, 200);
    } catch (\Exception $e) {
      $response = [
        'status' => 'error',
        'message' => $e->getMessage(),
      ];
      return response()->json($response, 500);
    }
  }
}
