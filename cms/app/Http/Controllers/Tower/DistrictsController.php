<?php

namespace App\Http\Controllers\Tower;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Districts;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;

class DistrictsController extends Controller
{
    public function index(Request $request)
    {
      try {
        $param = $request->all();
        $query = Districts::select('ID','KECAMATAN','Luas_Ha','KODE_KEC',\DB::raw('AsText(ogc_geom) as ogc_geom'))
                            ->where(function ($result) use ($param) {
                              if (!empty($param['referral_code'])) {
                                $result->where('referral_code', 'ILIKE', '%' . $param['referral_code'] . '%');
                              }
                              if (!empty($param['branch_name'])) {
                                $result->where('branch_name', 'ILIKE', '%' . $param['branch_name'] . '%');
                              }
                            })
                            ->orderby('id', 'desc')
                            ->paginate($param['perpage']);
        $response = [
          'status' => 'success',
          'message' => 'Proses berhasil',
          'getdata' => $query,
        ];
        return response()->json($response, 200);
      } catch (\Exception $e) {
        $response = [
          'status' => 'error',
          'message' => $e->getMessage(),
        ];
        return response()->json($response, 500);
      }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
      try {
        $query = Districts::select('ID','KECAMATAN','Luas_Ha','KODE_KEC',\DB::raw('AsText(ogc_geom) as ogc_geom'))->first();
        $polygon = Polygon::fromWKT($query['ogc_geom']);
        $arr = ['type'=>"FeatureCollection",'features'=>[['type'=>'Feature','geometry'=>$polygon,'properties'=>['code'=>'12345','name'=>'Wiwitan']]]];
        return $arr;
      } catch (\Exception $e) {
        $response = [
          'status' => 'error',
          'message' => $e->getMessage(),
        ];
        return response()->json($response, 500);
      }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
