<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="robots" content="noindex,nofollow" />
    <title>Dashboard - Mazer Admin Dashboard</title>
    <link rel="shortcut icon" href="assets/images/favicon.svg" type="image/x-icon" />
  </head>

  <body>
    <div id="app">
      <App></App>
    </div>

    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/iconly/bold.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/bootstrap-icons/bootstrap-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/icons/simple-line-icons/css/simple-line-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/icons/themify-icons/themify-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/icons/dripicons/webfont.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/featherlight.min.css') }}" />
    <script src="{{ asset('/js/additional.js') }}"></script>
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{ asset('/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('/js/main.js') }}"></script>
    <script src="{{ asset('/js/featherlight.min.js') }}"></script>

  </body>
</html>
