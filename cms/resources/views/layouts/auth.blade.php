<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="robots" content="noindex,nofollow" />
    <title>Loading...</title>
  </head>

  <body>
    <div id="app">
      <App></App>
    </div>
    @php
      JavaScript::put([
        'myassets' => myFunction::getAssets(),
      ]);
    @endphp
    @include('footer')
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendors/bootstrap-icons/bootstrap-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/pages/auth.css') }}" />

    <link rel="stylesheet" href="{{ asset('/css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}" />
    <script src="{{ asset('/js/additional.js') }}"></script>
    <script src="{{ asset('/js/app.js') }}"></script>
  </body>
</html>
