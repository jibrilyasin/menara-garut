import Vue from "vue";
import Vuex from "vuex";
import VueSweetalert2 from 'vue-sweetalert2';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    myprofile: [],
    settoken: "",
  },
  getters: {
    myprofile: (state) => state.myprofile,
    settoken: (state) => state.settoken,
  },
  mutations: {
    FETCH_MYPROFILE(state, data) {
      state.myprofile = data;
    },
    FETCH_SETTOKEN(state, data) {
      state.settoken = data;
    },
  },
  actions: {
    postProfile({ commit, state }) {
      window.axios.defaults.headers.common["Authorization"] = `Bearer ${state.settoken}`;
      axios
        .get(baseurl+"/api/myaccount/getprofile")
        .then((response) => {
          commit("FETCH_MYPROFILE", response.data);
        })
        .catch(function (error) {
          swal("Info", error.message, "error");
        });
    },
    setMyToken({ commit, state }, accessToken) {
      commit("FETCH_SETTOKEN", accessToken);
    },
    deleteMyToken({ commit }) {
      commit("FETCH_SETTOKEN", "");
    },
  },
  plugins: [createPersistedState()],
});
