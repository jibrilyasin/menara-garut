import WrapDashboard from "../components/layouts/Dashboard";
import Dashboard from "../components/pages/Dashboard";
export default [
  {
    path: "/",
    component: WrapDashboard,
    children: [
      {
        name: "DashboardPage",
        path: "",
        component: Dashboard,
        props: {
          title: "Halaman Dashboard",
          page: "Halaman Dashboard",
          search: false,
        },
      },
    ],
  },
];
