import WrapDashboard from "../components/layouts/Dashboard";
import Distrticts from "../components/pages/tower/distrticts/Data";
import TowerInformation from "../components/pages/tower/information/Data";
export default [
  {
    path: "/tower",
    component: WrapDashboard,
    children: [
      {
        name: "DistrtictsPage",
        path: "districts",
        component: Distrticts,
        props: {
          title: "Halaman Kecamatan",
          page: "Halaman Kecamatan"
        },
      },
      {
        name: "TowerInformationPage",
        path: "information",
        component: TowerInformation,
        props: {
          title: "Halaman Informasi Tower",
          page: "Halaman Informasi Tower"
        },
      },
    ],
  },
];
