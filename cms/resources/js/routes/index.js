import Vue from 'vue'
import Router from 'vue-router'
import auth from './auth';
import general from './general';
import tower from './tower';

Vue.use(Router)
const router = new Router({
  //base: baseurl,
  mode: "history",
  routes: [
		...auth,
    ...general,
    ...tower
	]
})
export default router;
