import WrapAuth from "../components/layouts/Auth";
import Login from '../components/pages/auth/Login';

export default [
	{
	    path: '/auth',
      name:'WrapAuth',
	    component: WrapAuth,
	    children: [
	        {
	            name: "LoginPage",
	            path: "login",
	            component: Login,
	            props: {
	                title: "Halaman Login",
	                page: "Halaman Login",
	            },
	        },
	    ],
	},
]
