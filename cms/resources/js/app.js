import "./bootstrap";
import Vue from 'vue';
import Routes from './routes';
import VueMeta from 'vue-meta'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import vWow from 'v-wow';
import VueMoment from 'vue-moment';
import JsonExcel from "vue-json-excel";
import VueToast from 'vue-toast-notification';
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';
import 'vue-toast-notification/dist/theme-sugar.css';
import App from './components/App';
import store from './store.js';
import 'bootstrap'

Vue.use(VueMeta);
Vue.use(VueToast);
Vue.use(VueMoment);
Vue.use(VueSweetalert2);
Vue.use(vWow);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component("downloadExcel", JsonExcel);

window.baseurl = window.location.origin;

const app = new Vue({
	el:'#app',
	components: { App },
  router : Routes,
  store,
});
