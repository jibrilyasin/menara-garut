const preloaderpage = function() {
    $(".preloader").fadeIn('fast');
};
const afterpreloaderpage = function() {
    $(".preloader").fadeOut('400');
};

const formatCurrency=function(total){
    var result=total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return result;
}
const formatCurrencyDot=function(total){
    var result=total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    return result;
}
const removeFormatCurrency = function(total) {
    return total.replace(/[.,\s]/g, '');
}
const ValidURL = function(str) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if(!regex .test(str)) {
      return "false";
    } else {
      return "true";
    }
}
